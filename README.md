![Screenshot of a realistic messy teenage-room desk in a warm room. A pinboard is labelled with "mind board" and has an "order thoughts" button.](gitlab-banner-screenshot.png)

# Frame of Mind
Imagine moving out of your childhood room. What mementos will you find? What thoughts will they bring with them? What do you want to take away?

And how will all of it have changed by the time you move next?

## Description
Frame of Mind is a queer game about mental health.

## Installation
Get [Godot](https://godotengine.org/download) and open the src folder as a new project.

## Building and developing with Nix
### Building
This project can be build with `$ nix-build` or `$ nix build` (flakes).
### Building for Linux without Nix
Use `$ nix bundle`, the resulting binary should be completly self contained.
### Developing
It is recommended to use `direnv`, but `$ nix develop`, `$ nix shell` and `$ nix-shell` also work.

## Support Me
You can support the development of Frame of Mind by
 - [Becoming a Patreon](https://www.patreon.com/betalars)
 - [Contacting me to Cotnribute Code or Assets](mailto::contact@betalars.de)

## License
 - [CC-BY-SA-NC](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.de)

## Project status
Currently working on an UX overhaul based on feedback I have gotten on a trade show. Pushing myself to participate in the upcoming steam next fest.
