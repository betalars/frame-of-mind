## Summary

(This template will help you publicly describe an issue you have encountered. Contact email support, when you do not with to openly describe your Issue.)

(Fill out what is relevant to your issue and delete what's not)

## Steps to reproduce
(What did you do before finding the issue? Providing a step by step guide can help us solve it. Try if the issue still persists after restarting the game.)

Example
1. Load the first Level
2. Go to the Laptop and click the screen 4 times
3. Run at the window.

### What did you expect to happen?
(The player hits the window)

### What happend?
(The player floats up towards the sky)

## Did you change any settings?

(We will provide you with an option to just dump your config here in the future, but for now just describe what you did.)

## How do you play the game?

(With a racing wheel and a touch screen, obviousely)

## Please give us some System Info

- OS: GameOS 42
- Processor: intel Ryzen 1234XD
- Graphics Card: Radeon Battlemage Pro

## Possible Fixes
(This is the section for nerds, that already dug up our source code and know exactly what is going on.)

/label ~bug