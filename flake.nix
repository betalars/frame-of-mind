{
  description = "Frame of Mind";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    flake-compat,
  }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        pkgs = import nixpkgs {
          inherit system;

          overlays = [
            (self: super: {
              godot = super.godot_4;

              godot-export-templates =
                (super.godot_4.overrideAttrs (old: let
                  version = builtins.replaceStrings ["-"] ["."] old.version;
                in {
                  pname = "godot-export-templates";

                  installPhase = ''
                    # The godot export command expects the export templates at
                    # .../share/godot/export_templates/4.0.stable/linux_release.x86_64 with 4.0_stable being the godot version.
                    mkdir -p "$out/share/godot/export_templates/${version}"
                    cp bin/godot* "$out/share/godot/export_templates/${version}/linux_release.x86_64"
                  '';

                  # https://docs.godotengine.org/en/stable/development/compiling/optimizing_for_size.html
                  # Stripping reduces the template size from around 500MB to 40MB for Linux.
                  # This also impacts the size of the exported games.
                  # This is added explicitly here because mkDerivation does not automatically
                  # strip binaries in the template directory.
                  stripAllList = (old.stripAllList or []) ++ ["share/godot/export_templates"];

                  outputs = ["out"];

                  meta.description = "Free and Open Source 2D and 3D game engine (export templates)";
                }))
                .override
                {
                  withTarget = "template_release";
                };
            })
          ];
        };
      in rec {
        packages = rec {
          frame-of-mind = pkgs.stdenv.mkDerivation rec {
            pname = "frame-of-mind";
            version = "0.0.0";
            src = ./src;
            src-logo = ./design/smol_logo.png;
            dontUnpack = true;

            nativeBuildInputs = with pkgs; [
              copyDesktopItems
              godot
            ];

            runtimeDependencies = pkgs.godot.runtimeDependencies;

            buildPhase = ''
              runHook preBuild

              cp -r ${src}/* ./

              # Cannot create file '/homeless-shelter/.config/godot/projects/...'
              export HOME=$TMPDIR

              # Link the export-templates to the expected location. The --export commands
              # expects the template-file at .../templates/<version>/linux_x11_64_release
              mkdir -p $HOME/.local/share/godot
              ln -sr ${pkgs.godot-export-templates}/share/godot/export_templates $HOME/.local/share/godot/export_templates

              mkdir -p $out/share/${pname}
              # This just copies the export template and packs the game files next to it
              godot4 --headless --export-release "linux" $out/share/${pname}/${pname}

              runHook postBuild
            '';

            installPhase = ''
              runHook preInstall

              mkdir -p $out/bin
              ln -s $out/share/${pname}/${pname} $out/bin

              mkdir -p $out/share/pixmaps
              cp ${src-logo} $out/share/pixmaps/${pname}.png

              runHook postInstall
            '';

            postFixup = ''
              # Godot has some "optional" dependencies like display drivers
              patchelf --set-rpath ${pkgs.lib.makeLibraryPath runtimeDependencies} $out/share/${pname}/${pname}
            '';

            desktopItems = [
              (pkgs.makeDesktopItem {
                name = "Frame of Mind";
                exec = pname;
                icon = pname;
                desktopName = "Frame of Mind";
                comment = "";
                genericName = "";
                categories = ["Game"];
              })
            ];
          };
        };
        devShell = pkgs.mkShell {
          nativeBuildInputs = with pkgs; [
            godot
            godot-export-templates
            blender
            inkscape
          ];

          # FIXME
          shellHook = ''
            GODOT_EXPORT_TEMPLATE=${pkgs.godot-export-templates}/share/godot/templates/*/linux_x11_64_release
          '';
        };
        defaultPackage = packages.frame-of-mind;
      }
    );
}
