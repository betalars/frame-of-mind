extends CenterContainer


# Called when the node enters the scene tree for the first time.
func _ready():
    $cards/card_1/AnimationPlayer.play("select")
    await $cards/card_1/AnimationPlayer.animation_finished
    $cards/card_1/AnimationPlayer.play("deselect")
    $cards/card_2/AnimationPlayer.play("select")
    await $cards/card_1/AnimationPlayer.animation_finished
    $cards/card_2/AnimationPlayer.play("deselect")
    $cards/card_3/AnimationPlayer.play("select")
    await $cards/card_2/AnimationPlayer.animation_finished
    $cards/card_3/AnimationPlayer.play("deselect")
    await $cards/card_3/AnimationPlayer.animation_finished
    $cards/card_1/AnimationPlayer.play("select")
    await $cards/card_1/AnimationPlayer.animation_finished
    $cards/card_1/AnimationPlayer.play("take")
    $cards/card_2/AnimationPlayer.play("unshuffle")
    $cards/card_3/AnimationPlayer.play("shuffle")
    await $cards/card_3/AnimationPlayer.animation_finished
    
    for card in $cards.get_children():
        card.get_child(1).play("RESET")
        
    $cards/card_2/AnimationPlayer.queue("select")
    await $cards/card_2/AnimationPlayer.animation_finished
    $cards/card_2/AnimationPlayer.play("take")
    $cards/card_3/AnimationPlayer.play("unshuffle")
    $cards/card_1/AnimationPlayer.play("shuffle")
    await $cards/card_1/AnimationPlayer.animation_finished
    
    for card in $cards.get_children():
        card.get_child(1).play("RESET")
        
    $cards/card_3/AnimationPlayer.queue("select")
    await $cards/card_3/AnimationPlayer.animation_finished
    $cards/card_3/AnimationPlayer.play("take")
    $cards/card_1/AnimationPlayer.play("unshuffle")
    $cards/card_2/AnimationPlayer.play("shuffle")
    await $cards/card_2/AnimationPlayer.animation_finished
    
    $postIts.show()
    
    for card in $postIts.get_children():
        card.get_child(1).play("post")
    
    await $postIts/postIt_1/AnimationPlayer.animation_finished
    
    $postIts/postIt_1/AnimationPlayer.play("select")
    await $postIts/postIt_1/AnimationPlayer.animation_finished
    $postIts/postIt_1/AnimationPlayer.play("deselect")
    $postIts/postIt_2/AnimationPlayer.play("select")
    await $postIts/postIt_2/AnimationPlayer.animation_finished    
    $postIts/postIt_2/AnimationPlayer.play("deselect")
    $postIts/postIt_3/AnimationPlayer.play("select")
    await $postIts/postIt_3/AnimationPlayer.animation_finished    
    $postIts/postIt_3/AnimationPlayer.play("deselect")
    $postIts/postIt_4/AnimationPlayer.play("select")
    await $postIts/postIt_4/AnimationPlayer.animation_finished    
    $postIts/postIt_4/AnimationPlayer.play("deselect")
    $postIts/postIt_1/AnimationPlayer.play("select")
    await get_tree().create_timer(1).timeout
    $postIts/postIt_1/AnimationPlayer.play("pick")
    $postIts/postIt_2/AnimationPlayer.play("shuffle")
    $postIts/postIt_3/AnimationPlayer.play("unshuffle")
    $postIts/postIt_4/AnimationPlayer.play("unshuffle")
    
    await $postIts/postIt_2/AnimationPlayer.animation_finished    
    
    for card in $postIts.get_children():
        card.get_child(1).play("post")
    
    await $postIts/postIt_1/AnimationPlayer.animation_finished
    
    $postIts/postIt_2/AnimationPlayer.play("select")
    await get_tree().create_timer(1).timeout
    $postIts/postIt_2/AnimationPlayer.play("pick")
    $postIts/postIt_3/AnimationPlayer.play("shuffle")
    $postIts/postIt_4/AnimationPlayer.play("unshuffle")
    $postIts/postIt_1/AnimationPlayer.play("unshuffle")

    await $postIts/postIt_3/AnimationPlayer.animation_finished

    for card in $postIts.get_children():
        card.get_child(1).play("post")
    
    await $postIts/postIt_1/AnimationPlayer.animation_finished
    
    $postIts/postIt_3/AnimationPlayer.play("select")
    await get_tree().create_timer(1).timeout
    $postIts/postIt_3/AnimationPlayer.play("pick")
    $postIts/postIt_4/AnimationPlayer.play("shuffle")
    $postIts/postIt_1/AnimationPlayer.play("unshuffle")
    $postIts/postIt_2/AnimationPlayer.play("unshuffle")
    
    await $postIts/postIt_4/AnimationPlayer.animation_finished
    
    for card in $postIts.get_children():
        card.get_child(1).play("post")
    
    await $postIts/postIt_1/AnimationPlayer.animation_finished
    
    $postIts/postIt_4/AnimationPlayer.play("select")
    await get_tree().create_timer(1).timeout
    $postIts/postIt_4/AnimationPlayer.play("pick")
    $postIts/postIt_1/AnimationPlayer.play("shuffle")
    $postIts/postIt_2/AnimationPlayer.play("unshuffle")
    $postIts/postIt_3/AnimationPlayer.play("unshuffle")
