extends Node3D

# Referencing the current State of the Scene.
enum Modes {
    FREEZE,
    WALKING,
    COLLECTING,
    LISTENING,
    SORTING
}

signal freeze
signal unfreeze
signal ini_room
signal resume_room

var current_mode: int = Modes.FREEZE:
    set(new_mode):
        if not current_mode == new_mode:
            current_mode = _update_scene(new_mode)

func start():
    emit_signal("ini_room")
    current_mode = Modes.WALKING
    
func _update_scene(new_mode) -> int:
    if current_mode == Modes.FREEZE:
        emit_signal("freeze")
    elif new_mode == Modes.FREEZE:
        emit_signal("freeze")

    return new_mode

func get_ready():
    self.show()
    $sfx/distant_rain.play()
    $"sfx/rain on window".play()
    await get_tree().create_timer(0.1).timeout
    $logic/UI/board.hide()
