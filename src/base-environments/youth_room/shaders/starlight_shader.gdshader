shader_type spatial;
render_mode blend_add, specular_disabled, ambient_light_disabled;

uniform float nebula_brightness = 4;
uniform vec2 nebula_scale = vec2(1.5);
uniform sampler2D starlight_noise;
uniform sampler2D starlight_textures;
uniform float rotation_speed = 0.02;
uniform vec2 rotation_pivot = vec2(.8);
uniform vec2 drift_compensation = vec2(0.1, -0.2);
uniform float noise_strength = 0.2;

// https://gist.github.com/ayamflow/c06bc0c8a64f985dd431bd0ac5b557cd
vec2 rotateUV(vec2 uv, vec2 pivot, float rotation)
{
    return vec2(
      cos(rotation) * (uv.x - pivot.x) + sin(rotation) * (uv.y - pivot.y) + pivot.x,
      cos(rotation) * (uv.y - pivot.y) - sin(rotation) * (uv.x - pivot.x) + pivot.y
    );
}

//clamp(, .0, 1.0

void fragment() {
	ALBEDO = vec3(.0,
                  texture(starlight_textures,UV*1.5).x * 2.0*pow(max(sin(TIME + 10.0 * texture(starlight_textures,UV).y), .0), 5.0),
                  texture(starlight_textures,
                            clamp(
                                UV / nebula_scale + drift_compensation + 1.0 *
                                    texture(starlight_noise, rotateUV( 
                                                                UV / nebula_scale + noise_strength * texture(starlight_noise, rotateUV(UV, -rotation_pivot, TIME*rotation_speed)).yz,
                                                                rotation_pivot, TIME*rotation_speed)
                                            ).xz,
                                    .0, 1.0))
                                .z * nebula_brightness);
    SPECULAR = 0.0;
}
